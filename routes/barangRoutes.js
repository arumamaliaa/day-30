const express = require("express");
const router = express.Router();
const barangValidator = require("../middlewares/validators/barangValidator");
const barangController = require("../controllers/barangController");

router.get("/", barangController.getAll);
router.get("/:id", barangValidator.getOne, barangController.getOne);
router.post("/", barangValidator.create, barangController.create);
router.put("/:id", barangValidator.update, barangController.update);
router.delete("/:id", barangValidator.delete, barangController.delete);

module.exports = router;
