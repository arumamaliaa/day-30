const crypto = require("crypto");
const path = require("path");
const mongoose = require("mongoose");
const validator = require("validator");
const { barang, pelanggan, pemasok, transaksi } = require("../../models");

exports.getOne = (req, res, next) => {

  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res.status(400).json({
      message: "Parameter is not valid and must be 24 characters & hexadecimal",
    });
  }

  next();
};

exports.create = async (req, res, next) => {
  try {
    let errors = [];

    if (!mongoose.Types.ObjectId.isValid(req.body.id_pemasok)) {
      errors.push(
        "id_pemasok is not valid and must be 24 characters & hexadecimal",
      );
    }

    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    let findData = await Promise.all([
      barang.findOne({ nama: req.body.nama }),
      pemasok.findOne({ _id: req.body.id_pemasok }),
    ]);

    if (findData[0]) {
      errors.push("Pemasok not found");
    }

    // If data pemasok not found
    if (!findData[1]) {
      errors.push("Pemasok not found");
    }


    if (!validator.isNumeric(req.body.harga)) {
      errors.push("Harga must be a number");
    }

    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    if (req.files) {
      const file = req.files.image;

      if (!file.mimetype.startsWith("image")) {
        errors.push("File must be an image");
      }

      if (file.size > 1000000) {
        errors.push("Image must be less than 1MB");
      }

      let fileName = crypto.randomBytes(16).toString("hex");

      file.name = `${fileName}${path.parse(file.name).ext}`;

      req.body.image = file.name;

      file.mv(`./public/images/${file.name}`, async (err) => {
        if (err) {
          console.log(err);

          return res.status(500).json({
            message: "Internal Server Error",
            error: err.message,
          });
        }
      });
    }

    req.body.pemasok = req.body.id_pemasok;

    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.update = async (req, res, next) => {
  let errors = [];

  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    errors.push(
      "id_barang is not valid and must be 24 characters & hexadecimal",
    );
  }

  if (!mongoose.Types.ObjectId.isValid(req.body.id_pemasok)) {
    errors.push(
      "id_pemasok is not valid and must be 24 characters & hexadecimal",
    );
  }

  if (errors.length > 0) {
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  let dataPemasok = await pemasok.findOne({ _id: req.body.id_pemasok });

  if (!dataPemasok) {
    errors.push("Pemasok not found");
  }

  if (!validator.isNumeric(req.body.harga)) {
    errors.push("Harga must be a number");
  }

  if (errors.length > 0) {
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  req.body.pemasok = dataPemasok;

  next();
};

exports.delete = async (req, res, next) => {
  let errors = [];

  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    errors.push(
      "id_barang is not valid and must be 24 character & hexadecimal"
    );
  }

  if (errors.length > 0) {
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  let data = await barang.findOne({ _id: req.params.id });

  // If transaksi not found
  if (!data) {
    errors.push("Barang not found");
  }

  if (errors.length > 0) {
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  next();
};
