const validator = require("validator");
const mongoose = require("mongoose");
const { barang, pelanggan, pemasok, transaksi } = require("../../models");

exports.getOne = (req, res, next) => {
    try {

        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
            return res.status(400).json({
                message: "Parameter is not valid and must be 24 characters & hexadecimal",
            });
        }

        next();
    } catch (e) {
        return res.status(500).json({
            message: "Internal Server Error",
            error: e.message,
        });
    }
};

exports.create = async (req, res, next) => {
    try {
        let errors = [];

        if (!mongoose.Types.ObjectId.isValid(req.body.id_barang)) {
            return res.status(400).json({
                message: "id_barang is not valid and must be 24 characters & hexadecimal",
            });
        }

        if (!mongoose.Types.ObjectId.isValid(req.body.id_pelanggan)) {
            return res.status(400).json({
                message: "id_pelanggan is not valid and must be 24 characters & hexadecimal",
            });
        }

        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        let findData = await Promise.all([
            barang.findOne({ _id: req.body.id_barang }),
            pelanggan.findOne({ _id: req.body.id_pelanggan }),
        ]);

        if (!findData[0]) {
            errors.push("Barang not found");
        }

        if (!findData[1]) {
            errors.push("Pelanggan not found");
        }

        if (!validator.isNumeric(req.body.jumlah)) {
            errors.push("Jumlah must be a number");
        }

        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        req.body.barang = findData[0];
        req.body.pelanggan = findData[1];
        req.body.total = eval(findData[0].harga * req.body.jumlah);

        next();
    } catch (e) {
        return res.status(500).json({
            message: "Internal Server Error",
            error: e.message,
        });
    }
};

exports.update = async (req, res, next) => {
    try {
        let errors = [];

        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
            errors.push(
                "id_transaksi is not valid and must be 24 characters & hexadecimal",
            );
        }

        if (!mongoose.Types.ObjectId.isValid(req.params.id_barang)) {
            errors.push(
                "id_barang is not valid and must be 24 characters & hexadecimal",
            );
        }

        if (!mongoose.Types.ObjectId.isValid(req.params.id_pelanggan)) {
            errors.push(
                "id_pelanggan is not valid and must be 24 characters & hexadecimal",
            );
        }

        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        let findData = await Promise.all([
            barang.findOne({ _id: req.body.id_barang }),
            pelanggan.findOne({ _id: req.body.id_pelanggan }),
            transaksi.findOne({ _id: req.params.id }),
        ]);

        if (!findData[0]) {
            errors.push("Barang not found");
        }

        if (!findData[1]) {
            errors.push("Pelanggan not found");
        }

        if (!findData[2]) {
            errors.push("Transaksi not found");
        }

        if (!validator.isNumeric(req.body.jumlah)) {
            errors.push("Jumlah must be a number");
        }

        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        req.body.barang = findData[0];
        req.body.pelanggan = findData[1];
        req.body.total = eval(findData[0].harga * req.body.jumlah);

        next();
    } catch (e) {
        return res.status(500).json({
            message: "Internal Server Error",
            error: e.message,
        });
    }
};

exports.delete = async (req, res, next) => {
    try {
        let errors = [];

        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
            errors.push(
                "id_transaksi is not valid and must be 24 character & hexadecimal"
            );
        }

        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        let data = await transaksi.findOne({ _id: req.params.id });

        // If transaksi not found
        if (!data) {
            errors.push("Transaksi not found");
        }

        if (errors.length > 0) {
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        next();
    } catch (e) {
        return res.status(500).json({
            message: "Internal Server Error",
            error: e.message,
        });
    }
};